<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Depense;
use App\Repository\CategoryRepository;
use App\Repository\DepenseRepository;
use App\Repository\UserRepository;
use Doctrine\DBAL\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class MainController extends AbstractController
{

    /**
     * @Route("/expenses", name="expenses", methods={"GET"})
     * @param DepenseRepository $depenseRepository
     * @return Response
     */
    public function expense(DepenseRepository $depenseRepository): Response
    {
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));

        $oui = $depenseRepository->findAll();

        $normalizer = new ObjectNormalizer($classMetadataFactory);
        $serializer = new Serializer([$normalizer]);

        $data = $serializer->normalize($oui,'null', ['groups' => 'group1']);
        // $data = ['foo' => 'foo'];

        dump($data);
        return $this->json($data);
    }

    /**
     * @Route("/categories", name="categories", methods={"GET"})
     * @param CategoryRepository $categoryRepository
     * @return Response
     */
    public function category(CategoryRepository $categoryRepository): Response
    {
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));

        $categ = $categoryRepository->findAll();

        $normalizer = new ObjectNormalizer($classMetadataFactory);
        $serializer = new Serializer([$normalizer]);

        $data = $serializer->normalize($categ,'null', ['groups' => 'group1']);

        dump($data);
        return $this->json($data);
    }

    /**
     * @Route("/newexpense", name="newexpense", methods={"POST"})
     * @return Response
     */
    public function addExpense(Request $request, UserRepository $userRepository, CategoryRepository $categoryRepository): Response
    {
        $data = $request->getContent();
        $relation = $request->toArray();

        try {
            $object = $this->get('serializer')->deserialize($data, Depense::class, 'json');
        }catch (Exception $e){
            echo 'Exception reçue : ',  $e->getMessage(), "\n";
        }
        $user = $userRepository->findOneBy(array('id' => $relation['user'] ));
        $categ = $categoryRepository->findOneBy(array('id' => $relation['category']));

        $object->setUser($user);
        $object->setCategory($categ);


        dump($data);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($object);
        $entityManager->flush();

        return new Response(
            '<html><body>ajout de données</body></html>'
        );

    }

    /**
     * @Route("/newcateg", name="newcateg", methods={"POST"})
     * @return Response
     */
    public function addCateg(Request $request, UserRepository $userRepository, CategoryRepository $categoryRepository): Response
    {
        $data = $request->getContent();

        try {
            $object = $this->get('serializer')->deserialize($data, Category::class, 'json');
        }catch (Exception $e){
            echo 'Exception reçue : ',  $e->getMessage(), "\n";
        }

        dump($data);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($object);
        $entityManager->flush();

        return new Response(
            '<html><body>ajout de données</body></html>'
        );

    }
}
